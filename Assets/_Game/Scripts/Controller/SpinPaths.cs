﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinPaths : MonoBehaviour
{
    public List<GameObject> paths_1 = new List<GameObject>();
    public List<GameObject> paths_2 = new List<GameObject>();

    private List<BoxCollider> listBoxCollider = new List<BoxCollider>();

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            paths_1.Add(transform.GetChild(i).gameObject);

            BoxCollider boxCollider = transform.GetChild(i).gameObject.AddComponent<BoxCollider>();
            boxCollider.isTrigger = true;
            listBoxCollider.Add(boxCollider);
        }

        for(int i = transform.childCount - 1; i >= 0; i--)
        {
            paths_2.Add(transform.GetChild(i).gameObject);
        }

        transform.GetChild(0).gameObject.tag = "Path 1";
        transform.GetChild(transform.childCount - 1).gameObject.tag = "Path 2";

        
    }

    public void HideBoxCollider()
    {
        for(int i = 0; i < listBoxCollider.Count; i++)
        {
            listBoxCollider[i].enabled = false;
        }
    }

    public void ActiveBoxCollider()
    {
        Invoke("DelayActiveBoxCollider", 1);
    }

    private void DelayActiveBoxCollider()
    {
        for (int i = 0; i < listBoxCollider.Count; i++)
        {
            listBoxCollider[i].enabled = true;
        }
    }
}
  
