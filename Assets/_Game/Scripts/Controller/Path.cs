﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    #region Editor Params
    public List<Transform> waypoints;
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods


    [NaughtyAttributes.Button]
    public void GetWayPoints()
    {
        foreach (Transform child in transform)
        {
            waypoints.Add(child.GetChild(0).transform);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (waypoints.Count > 0)
        {
            for (int i = 0; i < waypoints.Count - 1; i++)
            {
                Gizmos.DrawSphere(waypoints[i].position, 0.2f);
                Gizmos.DrawLine(waypoints[i].position, waypoints[i + 1].position);
            }
            Gizmos.DrawSphere(waypoints[waypoints.Count - 1].position, 0.2f);
        }
    }
    #endregion
}
