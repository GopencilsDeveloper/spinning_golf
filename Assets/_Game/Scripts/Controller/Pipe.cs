﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    private List<Vector3> paths_1 = new List<Vector3>();

    public List<GameObject> objectPaths_1 = new List<GameObject>();
    public List<GameObject> objectPaths_2 = new List<GameObject>();

    private BoxCollider startBoxCollider;
    private BoxCollider endBoxCollider;

    void Start()
    {
        PipeMeshGenerator pipeMeshGenerator = gameObject.GetComponent<PipeMeshGenerator>();
        paths_1 = pipeMeshGenerator.points;

        for (int i = 0; i < paths_1.Count; i++)
        {
            paths_1[i] += transform.localPosition;

            GameObject objectPosition = new GameObject();
            objectPosition.transform.SetParent(transform);
            objectPosition.transform.position = paths_1[i];
            objectPaths_1.Add(objectPosition);
        }

        startBoxCollider = objectPaths_1[0].AddComponent<BoxCollider>();
        startBoxCollider.center = Vector3.zero;
        startBoxCollider.size = Vector3.one;
        startBoxCollider.isTrigger = true;
        objectPaths_1[0].gameObject.tag = "Pipe 1";


        for (int i = objectPaths_1.Count-1;i >=0; i--)
        {
            objectPaths_2.Add(objectPaths_1[i]);
        }

        endBoxCollider = objectPaths_2[0].AddComponent<BoxCollider>();
        endBoxCollider.center = Vector3.zero;
        endBoxCollider.size = Vector3.one;
        endBoxCollider.isTrigger = true;
        objectPaths_2[0].gameObject.tag = "Pipe 2";
    }

    private void Update()
    {
        for (int i = 0; i < paths_1.Count - 1; i++)
        {
            Debug.DrawLine(paths_1[i], paths_1[i + 1]);
        }
    }
}
