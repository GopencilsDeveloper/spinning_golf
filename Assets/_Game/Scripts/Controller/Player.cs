﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float moveSpeed;

    [Header("Rotate In Pipe , Spin")]

    private List<GameObject> targetPaths = new List<GameObject>();

    private int number;
    private Vector3 targetPosition;
    private SpinPaths spinPaths;
    private Pipe pipeScript;
    public bool isAutoRotate;
    private bool isPaths1;
    private bool isBounce;
    private bool inPipe;
    private Rigidbody rigbody;

    private void Start()
    {
        rigbody = GetComponent<Rigidbody>();
        SetVelocity();
    }

    void Update()
    {
        MoveControl();
    }

    private void FixedUpdate()
    {
        MoveVelocity();
    }

    public void SetVelocity()
    {
        rigbody.velocity = transform.forward * moveSpeed;
    }

    private void MoveVelocity()
    {
        if (isAutoRotate)
            return;

        // move forward 
        //transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        rigbody.velocity = rigbody.velocity.normalized * moveSpeed;
    }

    private void MoveControl()
    {
        if (isAutoRotate)
        {
            targetPosition = targetPaths[number].transform.position;
            targetPosition.y = transform.position.y;

            float distance = Vector3.Distance(transform.position, targetPosition);
            if (distance > .3f)
            {
                // rotate forward
                Vector3 direction = targetPosition - transform.position;
                var targetQuaternion = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetQuaternion, Time.deltaTime * 30);

                // transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * moveSpeed);
                rigbody.velocity = direction.normalized * moveSpeed;
            }
            else
            {
                // update target path
                UpdateTargetPath();
            }
        }
     
    }

    private void UpdateTargetPath()
    {

        if (number < targetPaths.Count - 1)
        {
            number++;
            targetPosition = targetPaths[number].transform.position;
        }
        else
        {
            //SetVelocity();
            isAutoRotate = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bounce"))
        {
            Debug.Log("bounce");

            if (isBounce)
                return;

            isBounce = true;
            Invoke("SetBounce", 1.0f);

            if (isPaths1)
            {
                targetPaths = spinPaths.paths_2;
                isPaths1 = false;
            }
            else
            {
                targetPaths = spinPaths.paths_1;
                isPaths1 = true;
            }

            number = 0;
            targetPosition = targetPaths[number].transform.position;

            isAutoRotate = true;
        }

        if (collision.gameObject.CompareTag("Wall Reflect"))
        {
            Vector3 direction = collision.gameObject.transform.parent.GetChild(1).GetChild(0).position - collision.gameObject.transform.parent.GetChild(1).position;
            Quaternion quaterion = Quaternion.LookRotation(direction);
            transform.rotation = quaterion;
            rigbody.velocity = direction.normalized * moveSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Pipe 1") || other.CompareTag("Pipe 2"))
        {
            // contact pipe , refresh list player paths
            inPipe = !inPipe;

            if (inPipe == false)
                return;

            pipeScript = other.gameObject.transform.parent.GetComponent<Pipe>();

            if (other.CompareTag("Pipe 1"))
            {
                targetPaths = pipeScript.objectPaths_1;
            }
            else
            {
                targetPaths = pipeScript.objectPaths_2;
            }

            number = 0;
            targetPosition = targetPaths[number].transform.position;

            isAutoRotate = true;
        }

        if(other.CompareTag("Path 1") || other.CompareTag("Path 2"))
        {
            // contact spin path  , refresh list player paths

            transform.parent.SetParent(other.gameObject.transform.parent.parent);

            spinPaths = other.gameObject.transform.parent.GetComponent<SpinPaths>();

            spinPaths.HideBoxCollider();
            
            if(other.CompareTag("Path 1"))
            {
                targetPaths = spinPaths.paths_1;
                isPaths1 = true;
            }
            else
            {
                targetPaths = spinPaths.paths_2;
                isPaths1 = false;
            }
       
            number = 0;
            targetPosition = targetPaths[number].transform.position;

            isAutoRotate = true;
        }


        if(other.CompareTag("Mesh"))
        {
            if (isAutoRotate)
                return;

            Time.timeScale = 0;
            Debug.Log("___LOSE___");
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Mesh"))
        {
            transform.parent.SetParent(null);
            spinPaths.ActiveBoxCollider();
        }
    }

    private void SetBounce()
    {
        isBounce = false;
    }

}
