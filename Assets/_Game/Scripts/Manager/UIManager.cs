﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    [Header("MENU")]
    public GameObject Menu;

    [Header("INGAME")]
    public GameObject InGame;

    [Header("ENDGAME")]
    public GameObject EndGame;

    [Header("GAMEOVER")]
    public GameObject GameOver;

    [Header("OTHERS")]

    #endregion

    #region Params
    private static UIManager instance;

    #endregion

    #region Properties
    public static UIManager Instance { get => instance ?? GameObject.FindObjectOfType<UIManager>(); }
    #endregion

    #region Events
    #endregion

    #region Methods
    public void ShowMenu()
    {
        Menu.SetActive(true);
    }
    public void ShowInGame()
    {
        InGame.SetActive(true);
    }
    public void ShowEndGame()
    {
        EndGame.SetActive(true);
    }
    public void ShowGameOver()
    {
        GameOver.SetActive(true);
    }

    public void HideMenu()
    {
        Menu.SetActive(false);
    }
    public void HideInGame()
    {
        InGame.SetActive(false);
    }

    public void HideEndGame()
    {
        EndGame.SetActive(false);
    }
    public void HideGameOver()
    {
        GameOver.SetActive(false);
    }

    private void Awake()
    {
        GameManager.Instance.OnGameStateChangedHandler += OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Menu:
                ShowMenu();
                HideInGame();
                HideEndGame();
                HideGameOver();
                break;
            case GameState.InGame:
                HideMenu();
                ShowInGame();
                HideEndGame();
                HideGameOver();
                break;
            case GameState.EndGame:
                HideMenu();
                HideInGame();
                ShowEndGame();
                HideGameOver();
                break;
            case GameState.GameOver:
                HideMenu();
                HideInGame();
                HideEndGame();
                ShowGameOver();
                break;
            default:
                break;
        }
    }

    #endregion
}
