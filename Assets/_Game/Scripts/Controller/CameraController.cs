﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera cam;
    private void Start()
    {
        SetFOV();
    }

    void SetFOV()
    {
        float ratio = cam.aspect;
        if (ratio >= 0.75) // 3:4
        {
            Camera.main.fieldOfView = 50f;
        }
        else
        if (ratio >= 0.56) // 9:16
        {
            Camera.main.fieldOfView = 55;
        }
        else
        if (ratio >= 0.45) // 9:19
        {
            Camera.main.fieldOfView = 60;
        }
    }
}
