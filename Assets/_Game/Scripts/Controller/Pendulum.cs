﻿using UnityEngine;
using System.Collections;

public class Pendulum : MonoBehaviour
{
    #region Editor Params
    public float start;
    public float end;
    public float rotationY;
    #endregion

    #region Params
    private Vector3 tempRotation;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    void SetRotation(Vector3 angle)
    {
        transform.rotation = Quaternion.Euler(angle);
        tempRotation = transform.rotation.eulerAngles;
    }

    [NaughtyAttributes.Button]
    public void Exe()
    {
        Waggle(0f, 3f);
    }

    public void Waggle(float waggleDelay = 0f, float duration = 3f)
    {
        SetRotation(new Vector3(start, rotationY, 0f));
        StartCoroutine(C_Waggle(start, end, duration, waggleDelay));
    }

    IEnumerator C_Waggle(float start, float end, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);
        float t = 0f;
        while (t >= 0f)
        {
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempRotation.x = Mathf.Lerp(start, end, Mathf.SmoothStep(0f, 1f, t));
                transform.rotation = Quaternion.Euler(tempRotation);
                yield return null;
            }
            t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempRotation.x = Mathf.Lerp(end, start, Mathf.SmoothStep(0f, 1f, t));
                transform.rotation = Quaternion.Euler(tempRotation);
                yield return null;
            }
            t = 0f;
        }
    }

    #endregion
}
