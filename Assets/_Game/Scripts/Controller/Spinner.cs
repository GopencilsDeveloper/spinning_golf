﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    #region Editor Params
    public float speed;
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods
    public void Spin(int direction = 0, float speed = 5f) //direction:  0:Clockwise 1:!Clockwise
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            Spin();
        }
    }
    #endregion
}
