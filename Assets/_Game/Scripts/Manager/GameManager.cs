﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, Menu, InGame, EndGame, GameOver
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    public GameState currentState;

    private static GameManager instance;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance ?? GameObject.FindObjectOfType<GameManager>(); }
    #endregion

    #region Events
    public event System.Action<GameState> OnGameStateChangedHandler;
    #endregion

    #region Methods
    public void OnGameStateChanged(GameState gameState)
    {
        // print("OnGameStateChanged: " + gameState);
        currentState = gameState;
    }

    private void Awake()
    {
        OnGameStateChangedHandler += OnGameStateChanged;
    }

    public void ChangeGameState(GameState gameState)
    {
        if (OnGameStateChangedHandler != null)
        {
            OnGameStateChanged(gameState);
        }
    }

    public void StartGame()
    {
        ChangeGameState(GameState.Menu);
    }

    public void PlayGame()
    {
        ChangeGameState(GameState.InGame);
    }

    public void EndGame()
    {
        ChangeGameState(GameState.EndGame);
    }

    public void GameOver()
    {
        ChangeGameState(GameState.GameOver);
    }

    #endregion
}
